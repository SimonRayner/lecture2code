from Bio import SeqIO
from Bio.SeqUtils import GC

# Simple example to calculate GC percentage of human miRNAs in 
# most recent miRBase release (22)
miRBaseAll="/data/mirbase/22/one.fa"
gcPercent = 0.0
hsaCount = 0
for seqRec in SeqIO.parse(miRBaseAll, "fasta"):

    if "hsa" in seqRec.id:
        gcPercent = gcPercent + GC(seqRec.seq)
        print str(seqRec.seq) + "\t" + str(GC(seqRec.seq))
        hsaCount += 1

  
gcPercent = gcPercent/hsaCount
print "GC content is " + str(gcPercent) + "%"
