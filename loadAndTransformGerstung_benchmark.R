###############################################################################
# Before we try loading all the files, let's try loading a subset to see how
# long it takes
###############################################################################

# just load the libraries that we need here
library(affy)
library(gcrma)

library(microbenchmark)

# specify the path where we downloaded the data
gerstungFolder <- "/Users/simonray/Dropbox/teaching2018/Gerstung/Data/test7"

microbenchmark(
  celFiles <- dir(gerstungFolder, pattern = ".CEL", full.names = T),
  affyBatch <- read.affybatch(filenames = celFiles),
  gset = gcrma(affyBatch),
  times = 10
)




