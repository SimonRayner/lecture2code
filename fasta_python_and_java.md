---
title: "Program Design"
author: ""
output:
  pdf_document: default
  html_document: default
params:
  miRBaseHumanFA: /data/mirbase/22/hsa.mature.22.fa 
---
  

  
  
Now try calculating GC content using Python 
The file is saved as `calcGC.py` and can be run if you have `python` installed together with `BioPython` (i tested on a Mac with `Python 2.7.13` and `BioPython version 1.70`)


```
from Bio import SeqIO
from Bio.SeqUtils import GC

miRBaseAll="/data/mirbase/22/hsa.mature.22.fa"
seqs = SeqIO.read(miRBaseAll, "fasta")
for record in SeqIO.parse(miRBaseAll, "fasta"):
    if "hsa" in record.id:
      gc = gc + gc(seq)
      hsaCount += 1

  
gc = gc/hsaCount
```

when i run, i get 

```
>python calcGC.py
GC content is 52.3612983212%
```

This is pretty close to the first result we got from R, but it's still a little off, which seems odd. Let's not worry for now, and try `java`.


##Java

there is a `Biojava` package that corresponds to `BioPython` , but it's developed more for protein structure analysis. Also, there is a lot of overhead associated with using Biojava sequences. So, for this example, we will use the `BAGcore` packages we developed for miRNA analyses.

Here, we create one class `GCcontent` to load the fasta file and store the result of GC calculation. We use the `SimpleSequenceSet` class, which loads and stores a set of `SimpleSeq` instances. Both `SimpleSequenceSet` and `SimpleSeq` are part of the BAGcore package.

```
// java class to store GC content information

import no.uio.medisin.bag.core.sequence.SimpleSequenceSet;

public class GCcontent {
    SimpleSequenceSet simpleSeqSet;
    double gcFraction;
    
    public GCcontent(String filename){
        simpleSeqSet = new SimpleSequenceSet(new File(filename));
    }
}
```
and a run class to create an instance of the class and calculate the GC content
```
// java class to create instance of the GCcontent class

public class calcGCcontent {
    
    public static void main(String args[]){
        GCcontent gcContent = new GCcontent("/data/mirbase/22/mature.22.fa");   
        gcContent.gcFraction = 0.0;
        try{
            gcContent.simpleSeqSet.readFromFastaFile();
            for(SimpleSeq seq: gcContent.simpleSeqSet.getSeqs()){
                gcContent.gcFraction += seq.GCfraction();
            }
            System.out.print("GC content is " + gcContent.gcFraction + "%");
        }catch(java.io.IOException exIO){
            System.out.print("error reading fasta file");
        }
    }
```

when we run the compiled jar file


```
>java -jar ~/NetBeansProjects/CalcGC/target/CalcGC-1.0-SNAPSHOT-jar-with-dependencies.jar
GC content is 0.5238101266542544%
```

which is consistent with the `R/Biostrings` version. So, what's going on with the `R/seqinr` version, and should we be worried about the slight difference with the `python` code?

